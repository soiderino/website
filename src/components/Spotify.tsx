import { useLanyard } from 'react-use-lanyard';
import { DISCORD_ID } from '../utils/constants';

export const Spotify = () => {
    const { loading, status } = useLanyard({
        userId: DISCORD_ID,
        socket: true,
    });

    if (
        loading ||
        status === undefined ||
        !status?.listening_to_spotify ||
        status.spotify === undefined
    ) {
        return null;
    }

    function shorten(str: string | undefined, maxLen: number) {
        if (!str) return '';
        if (str.length <= maxLen) return str;
        return `${str.substring(0, maxLen - 3)}...`;
    }

    const spotifyTrackID = `https://open.spotify.com/track/${status.spotify.track_id}`;

    return (
        <>
            {status.listening_to_spotify ? (
                <div className="spotify">
                    <span
                        style={{
                            backgroundImage: `url(${status.spotify.album_art_url})`,
                        }}
                        className="spotify-banner"
                    ></span>
                    <div className="spotify-text">
                        <div className="spotify-title">
                            <a
                                target="_blank"
                                rel="noopener noreferrer"
                                href={spotifyTrackID}
                                title={status.spotify.song}
                            >
                                {shorten(status.spotify.song, 20)}
                            </a>
                        </div>
                        <div className="spotify-artist">
                            {status.spotify.artist.replaceAll('; ', ', ')}
                        </div>
                    </div>
                </div>
            ) : (
                <></>
            )}
        </>
    );
};
