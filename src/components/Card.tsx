import { age } from '../utils/birthday';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUpRightFromSquare } from '@fortawesome/free-solid-svg-icons';

import lv from '../assets/flags/LV.png';
import en from '../assets/flags/US.png';
import ru from '../assets/flags/RU.png';

export const Card = () => {
    const links = [
        {
            id: 1,
            name: 'Twitter',
            url: 'https://twitter.com/soiderino',
            title: 'Twitter',
            icon: <FontAwesomeIcon icon={faArrowUpRightFromSquare} />,
        },
        {
            id: 2,
            name: 'Github',
            url: 'https://github.com/soiderino',
            title: 'Github',
            icon: <FontAwesomeIcon icon={faArrowUpRightFromSquare} />,
        },
        {
            id: 3,
            name: 'Telegram',
            url: 'https://t.me/soiderino',
            title: 'Telegram',
            icon: <FontAwesomeIcon icon={faArrowUpRightFromSquare} />,
        },
    ];

    return (
        <div className="card">
            <div className="card-content">
                <div className="card-header">
                    Hello, I'm <span className="card-username">soiderino</span>
                </div>
                <div className="card-body">
                    <p>
                        I'm a {age} year old software engineer based in the
                        Latvia, I mostly specialize in web development, mainly
                        frontend development. Aside from programming, I enjoy
                        playing video games, watching anime and listening to
                        music.
                    </p>
                </div>
            </div>
            <hr />
            <div className="card-about">
                <div className="card-row">
                    <h3>Languages</h3>
                    <ul>
                        <li>
                            <img src={lv} alt="Latvia" />
                            Latvian (Native Speaker)
                        </li>
                        <li>
                            <img src={en} alt="United States" />
                            English (Intermediate)
                        </li>
                        <li>
                            <img src={ru} alt="Russia" />
                            Russian (Intermediate)
                        </li>
                    </ul>
                </div>
                <div className="card-row">
                    <h3>Links</h3>
                    <ul>
                        {links.map((link) => {
                            return (
                                <li key={link.id}>
                                    <a
                                        href={link.url}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        title={link.title}
                                    >
                                        {link.name}
                                        {link.icon}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        </div>
    );
};
