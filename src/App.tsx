import { Card } from './components/Card';
import { Spotify } from './components/Spotify';

function App() {
    return (
        <>
            <Spotify />
            <Card />
        </>
    );
}

export default App;
