/**
 * Represents a date of birth.
 */
let brithday = new Date('08/03/2003')

/**
 * Represents the current date.
 */
let now = new Date()

/**
 * Calculates the age based on the given birthday date.
 * @param birthday - The date of birth.
 * @returns The age calculated from the given birthday date.
 */
let age = now.getFullYear() - brithday.getFullYear()

export { age }
